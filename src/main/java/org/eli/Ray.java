package org.eli;

public class Ray {
    //源点
    private Vec3 origin;

    //方向
    private Vec3 direction;

    public Ray() {
        this.origin = new Vec3();
        this.direction = new Vec3();
    }

    public void setOrigin(Vec3 origin) {
        this.origin = origin;
    }

    public void setDirection(Vec3 direction) {
        this.direction = direction;
    }

    public Ray(Vec3 origin, Vec3 direction) {
        this.origin = origin;
        this.direction = direction;
    }

    public Vec3 origin() {
        return origin;
    }

    public Vec3 direction() {
        return direction;
    }

    public Vec3 at(double t) {
        return origin.add(direction.scale(t));
    }
}
