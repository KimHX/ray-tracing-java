package org.eli;

import org.eli.material.Material;

/**
 * 材质的指针
 */
public class HitRecord {

    // 撞击点的坐标
    public Vec3 p;

    // 撞击点的法向量
    public Vec3 normal;

    // 相撞的时间
    public double t;

    // 是否正面
    public boolean frontFace;

    // 材料
    public Material matPtr;

    public HitRecord() {
    }

    public HitRecord(Vec3 p, Vec3 normal, double t) {
        this.p = p;
        this.normal = normal;
        this.t = t;
    }

    public void setFaceNormal(Ray r, Vec3 outwardNormal) {
        this.frontFace = r.direction().dot(outwardNormal) < 0;
        this.normal = frontFace ? outwardNormal : outwardNormal.negate();
    }
}
