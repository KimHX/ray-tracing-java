package org.eli;

public class RtWeekend {
    // Constants

    public static final double INFINITY = Double.POSITIVE_INFINITY;
    public static final double PI = Math.PI;

    // Utility Functions

    public static double degreesToRadians(double degrees) {
        return degrees * PI / 180;
    }

    public static double ffmin(double a, double b) {
        return Math.min(a, b);
    }

    public static double ffmax(double a, double b) {
        return Math.max(a, b);
    }

    public static double randomDouble() {
        return Math.random();
    }

    public static double randomDouble(double min, double max) {
        return min + (max - min) * Math.random();
    }

    public static double clamp(double x, double min, double max) {
        if (x < min) {
            return min;
        }
        return Math.min(x, max);
    }

    public static double schlick(double cosine, double refIdx) {
        double r0 = (1 - refIdx) / (1 + refIdx);
        r0 = r0 * r0;
        return r0 + (1 - r0) * Math.pow((1 - cosine), 5);
    }
}
