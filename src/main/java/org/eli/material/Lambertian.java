package org.eli.material;

import org.eli.HitRecord;
import org.eli.Ray;
import org.eli.Vec3;

/**
 * 漫反射材质
 */
public class Lambertian implements Material {

    // 材料系数
    private Vec3 albedo;

    public Lambertian(Vec3 a) {
        this.albedo = a;
    }

    public Lambertian(Vec3 a, double f) {
        this.albedo = a;
    }

    @Override
    public boolean scatter(Ray rIn, HitRecord rec, Vec3 attenuation, Ray scattered) {
        Vec3 scatterDirection   = rec.normal.add(Vec3.randomUnitVector());
        scattered.setOrigin(rec.p);
        scattered.setDirection(scatterDirection);
        attenuation.e = albedo.e;
        return true;
    }
}
