package org.eli.material;

import org.eli.HitRecord;
import org.eli.Ray;
import org.eli.Vec3;

/**
 * 材料
 */
public interface Material {

    boolean scatter(Ray rIn, HitRecord rec, Vec3 attenuation, Ray scattered);
}
