package org.eli.material;

import org.eli.HitRecord;
import org.eli.Main;
import org.eli.Ray;
import org.eli.Vec3;

/**
 * 金属材质
 */
public class Metal implements Material {

    // 反射率
    private Vec3 albedo;

    // 镜面模糊
    private double fuzz;

    public Metal(Vec3 a) {
        this.albedo = a;
    }

    public Metal(Vec3 a, double f) {
        this.albedo = a;
        this.fuzz = Math.min(f, 1);
    }

    @Override
    public boolean scatter(Ray rIn, HitRecord rec, Vec3 attenuation, Ray scattered) {
        Vec3 reflected  = Vec3.reflect(Main.unitVector(rIn.direction()), rec.normal);
        scattered.setOrigin(rec.p);
        scattered.setDirection(reflected.add(Vec3.randomInUnitSphere().scale(fuzz)));
        attenuation.e = albedo.e;
        return scattered.direction().dot(rec.normal) > 0;
    }
}
