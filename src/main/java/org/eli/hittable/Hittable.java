package org.eli.hittable;

import org.eli.HitRecord;
import org.eli.Ray;

public interface Hittable {
    boolean hit(Ray r, double tMin, double tMax, HitRecord rec);
}
