package org.eli.hittable;

import org.eli.HitRecord;
import org.eli.Ray;

import java.util.ArrayList;
import java.util.List;

public class HittableList implements Hittable {
    private List<Hittable> objects;

    public HittableList() {
        objects = new ArrayList<>();
    }

    public HittableList(Hittable object) {
        add(object);
    }

    public void clear() {
        objects.clear();
    }

    public void add(Hittable object) {
        objects.add(object);
    }
    @Override
    public boolean hit(Ray r, double tMin, double tMax, HitRecord rec) {
        HitRecord tempRec = new HitRecord();
        boolean hitAnything = false;
        double closestSoFar = tMax;

        for (Hittable object : objects) {
            if (object.hit(r, tMin, closestSoFar, tempRec)) {
                hitAnything = true;
                closestSoFar = tempRec.t;
                rec.t = tempRec.t;
                rec.normal = tempRec.normal;
                rec.p = tempRec.p;
                rec.frontFace = tempRec.frontFace;
                rec.matPtr = tempRec.matPtr;
            }
        }

        return hitAnything;
    }
}
