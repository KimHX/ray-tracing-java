# ray-tracing-java

#### 介绍
使用纯Java实现一个光线追踪，无需引入任何第三方包，参考文献：Ray Tracing in One Weekend V3.0


实现效果：
![output.png](output.png)